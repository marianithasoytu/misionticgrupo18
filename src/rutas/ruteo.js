'use strict';

(function () {
    function init() {
        var router = new Router([
            new Route('home', 'inicio.html', true),
            new Route('catalog', 'catalogo.html'),
            new Route('procrear', 'crear.html'),
            new Route('prolistar', 'listado.html'),
            new Route('proadmin', 'admin.html'),
            new Route('login', 'acceso.html'),
            new Route('uuh', 'uuh.html')
        ]);
    }
    init();
}());
